<?php
/* Copyright (C) 2020-2022 Stephan Kreutzer
 *
 * This file is part of audio_messaging_system.
 *
 * audio_messaging_system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * audio_messaging_system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with audio_messaging_system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/client.php
 * @author Stephan Kreutzer
 * @since 2020-03-21
 */


 
require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");

require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("client"));

$parentEntryId = null;

if (isset($_GET['parent-id']) === true)
{
    $parentEntryId = (int)$_GET['parent-id'];
}

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <script type=\"text/javascript\" src=\"audio-recorder.js\"></script>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n".
     "        <div id=\"container\">\n".
     "          <div id=\"initLink\">\n".
     "            <a href=\"#\" onclick=\"initRecorder();\">".LANG_LINKCAPTION_INITIALIZERECORDER."</a>\n".
     "          </div>\n".
     "          <form action=\"#\" method=\"post\" id=\"formRecorder\">\n".
     "            <fieldset>\n".
     "              <button type=\"button\" id=\"buttonRecorderStart\" onclick=\"recorderStart(".$parentEntryId.");\" disabled=\"disabled\">".LANG_BUTTONCAPTION_RECORDSTART."</button>\n".
     "              <button type=\"button\" id=\"buttonRecorderStop\" onclick=\"recorderStop();\" disabled=\"disabled\">".LANG_BUTTONCAPTION_RECORDSTOP."</button><br/>\n".
     "              <input name=\"text\" maxlength=\"255\" type=\"text\" required=\"required\">\n".
     "              <input id=\"buttonComplete\" name=\"submit\" value=\"".LANG_BUTTONCAPTION_SUBMITSAVE."\" type=\"submit\" disabled=\"disabled\">\n".
     "            </fieldset>\n".
     "          </form>\n".
     "        </div>\n";

if ($parentEntryId != null)
{
    echo "        <div>\n".
         "          <a href=\"entry.php?id=".$parentEntryId."\">".LANG_LINKCAPTION_BACKTOENTRY."</a>\n".
         "        </div>\n";
}
else
{
    echo "        <div>\n".
         "          <a href=\"entry.php\">".LANG_LINKCAPTION_BACKTOENTRIES."</a>\n".
         "        </div>\n";
}

echo "      </div>\n".
     "    </div>\n".
     "    <div class=\"footerbox\">\n".
     "      <a href=\"license.php\" class=\"footerbox_link\">".LANG_LICENSE."</a>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n";

?>
