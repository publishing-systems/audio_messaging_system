<?php
/* Copyright (C) 2020-2022 Stephan Kreutzer
 *
 * This file is part of audio_messaging_system.
 *
 * audio_messaging_system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * audio_messaging_system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with audio_messaging_system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/playback.php
 * @author Stephan Kreutzer
 * @since 2020-03-28
 */



require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");

$entryId = null;

if (isset($_GET['id']) !== true)
{
    http_response_code(403);
    exit(0);
}

$entryId = (int)$_GET['id'];

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    header("HTTP/1.1 500 Internal Server Error");
    exit(-1);
}

$entry = Database::Get()->Query("SELECT `internal_file_name`,\n".
                                "    `mimetype`\n".
                                "FROM `".Database::Get()->GetPrefix()."entries`\n".
                                "WHERE `id`=?\n",
                                array($entryId),
                                array(Database::TYPE_INT));

if (is_array($entry) !== true)
{
    header("HTTP/1.1 500 Internal Server Error");
    exit(-1);
}

if (count($entry) <= 0)
{
    http_response_code(404);
    exit(0);
}

$path = "./recordings/".$entry[0]['internal_file_name'].".rec";

if (file_exists($path) !== true)
{
    header("HTTP/1.1 500 Internal Server Error");
    exit(-1);
}

header("Content-Type: ".$entry[0]['mimetype']);

readfile($path);



?>
