<?php
/* Copyright (C) 2012-2022 Stephan Kreutzer
 *
 * This file is part of audio_messaging_system.
 *
 * audio_messaging_system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * audio_messaging_system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with audio_messaging_system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/entry.php
 * @author Stephan Kreutzer
 * @since 2020-03-28
 */



require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");
require_once("./libraries/user_defines.inc.php");

require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("entry"));

$idEntry = null;

if (isset($_GET['id']) === true)
{
    $idEntry = (int)$_GET['id'];
}

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    header("HTTP/1.1 500 Internal Server Error");
    exit(-1);
}


if (isset($_POST['text']) === true &&
    isset($_POST['submit']) === true)
{
    if ($idEntry == null)
    {
        header("HTTP/1.1 400 Bad Request");
        exit(-1);
    }

    if (Database::Get()->BeginTransaction() !== true)
    {
        header("HTTP/1.1 500 Internal Server Error");
        exit(-1);
    }

    $idEntryRevision = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."entry_revisions` (`id`,\n".
                                               "    `text`,\n".
                                               "    `revision_datetime`,\n".
                                               "    `id_users`,\n".
                                               "    `id_entries`)\n".
                                               "VALUES (?, ?, UTC_TIMESTAMP(), ?, ?)",
                                               array(NULL, $_POST['text'], $_SESSION['user_id'], $idEntry),
                                               array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT));

    if ($idEntryRevision <= 0)
    {
        Database::Get()->RollbackTransaction();
        header("HTTP/1.1 500 Internal Server Error");
        exit(-1);
    }

    if (Database::Get()->CommitTransaction() !== true)
    {
        Database::Get()->RollbackTransaction();
        header("HTTP/1.1 500 Internal Server Error");
        exit(-1);
    }
}

$entries = null;

if ($idEntry !== null)
{
    $entries = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."entries`.`id` AS `entries_id`,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`internal_file_name` AS `entries_internal_file_name`,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`mimetype` AS `entries_mimetype`,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`status` AS `entries_status`,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id_users` AS `entries_id_users`,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id_entries` AS `entries_id_entries`,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`id` AS `entry_revisions_id`,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`text` AS `entry_revisions_text`,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` AS `entry_revisions_revision_datetime`,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_users` AS `entry_revisions_id_users`\n".
                                      "FROM `".Database::Get()->GetPrefix()."entry_revisions`\n".
                                      "RIGHT JOIN `".Database::Get()->GetPrefix()."entries` ON\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id` =\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_entries`\n".
                                      "WHERE `".Database::Get()->GetPrefix()."entries`.`status`=2 AND\n".
                                      "    (`".Database::Get()->GetPrefix()."entries`.`id`=? OR\n".
                                      "     `".Database::Get()->GetPrefix()."entries`.`id_entries`=?)\n".
                                      "ORDER BY `".Database::Get()->GetPrefix()."entries`.`id`=? DESC,\n".
                                      "    `".Database::Get()->GetPrefix()."entries`.`id` ASC,\n".
                                      "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` DESC",
                                      array($idEntry, $idEntry, $idEntry),
                                      array(Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT));

    if (is_array($entries) !== true)
    {
        header("HTTP/1.1 500 Internal Server Error");
        exit(-1);
    }

    if (count($entries) <= 0)
    {
        header("HTTP/1.1 404 Not Found");
        exit(-1);
    }

    if (((int)$entries[0]['entries_id']) != $idEntry)
    {
        header("HTTP/1.1 404 Not Found");
        exit(-1);
    }
}
else
{
    $entries = Database::Get()->QueryUnsecure("SELECT `".Database::Get()->GetPrefix()."entries`.`id` AS `entries_id`,\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`internal_file_name` AS `entries_internal_file_name`,\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`mimetype` AS `entries_mimetype`,\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`status` AS `entries_status`,\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`id_users` AS `entries_id_users`,\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`id_entries` AS `entries_id_entries`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`id` AS `entry_revisions_id`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`text` AS `entry_revisions_text`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` AS `entry_revisions_revision_datetime`,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_users` AS `entry_revisions_id_users`\n".
                                              "FROM `".Database::Get()->GetPrefix()."entry_revisions`\n".
                                              "RIGHT JOIN `".Database::Get()->GetPrefix()."entries` ON\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`id` =\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`id_entries`\n".
                                              "WHERE `".Database::Get()->GetPrefix()."entries`.`id_entries` IS NULL AND\n".
                                              "    `".Database::Get()->GetPrefix()."entries`.`status`=2\n".
                                              "ORDER BY `".Database::Get()->GetPrefix()."entries`.`id` ASC,\n".
                                              "    `".Database::Get()->GetPrefix()."entry_revisions`.`revision_datetime` DESC");

    if (is_array($entries) !== true)
    {
        header("HTTP/1.1 500 Internal Server Error");
        exit(-1);
    }
}

/*
if ($idEntry != null)
{
    if ((int)$entries[0]['entry_revisions_id_users'] !== (int)$_SESSION['user_id'] &&
        (int)$_SESSION['user_role'] !== USER_ROLE_ADMIN)
    {
        header("HTTP/1.1 403 Forbidden");
        exit(-1);
    }
}
*/


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
     "    <script type=\"text/javascript\" src=\"entry_controls.js\"></script>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n";

if ($idEntry !== null)
{
    if (is_numeric($entries[0]['entries_id_entries']) === true)
    {
        echo "        <div>\n".
             "          <a href=\"entry.php?id=".((int)$entries[0]['entries_id_entries'])."\">".LANG_LINKCAPTION_LEVELUP."</a>\n".
             "        </div>\n";
    }
    else
    {
        echo "        <div>\n".
             "          <a href=\"entry.php\">".LANG_LINKCAPTION_LEVELUP."</a>\n".
             "        </div>\n";
    }
}
else
{
    echo "        <div>\n".
         "          <a href=\"index.php\">".LANG_LINKCAPTION_MAIN."</a>\n".
         "        </div>\n";
}

if ($idEntry !== null)
{
    $lastEntryId = -1;

    foreach ($entries as $entry)
    {
        if ($lastEntryId != (int)$entry['entries_id'])
        {
            if ($lastEntryId >= 0)
            {
                break;
            }

            $lastEntryId = (int)$entry['entries_id'];

            echo "        <div id=\"update_control\"></div>\n";

            if (strlen($entry['entry_revisions_text']) > 0)
            {
                echo "        <span id=\"entry_text\">".htmlspecialchars($entry['entry_revisions_text'], ENT_XHTML, "UTF-8")."</span> <a href=\"playback.php?id=".$idEntry."\" target=\"_blank\">".LANG_LINKCAPTION_PLAYBACK."</a> <a href=\"#\" onclick=\"LoadInputControl(".$idEntry.");\">".LANG_LINKCAPTION_EDIT."</a> <a href=\"#\" onclick=\"ToggleRevisions();\">".LANG_LINKCAPTION_REVISIONS."</a>\n";
            }
            else
            {
                echo "        <span id=\"entry_text\">".LANG_ENTRYDEFAULTCAPTION."</span> <a href=\"playback.php?id=".$idEntry."\" target=\"_blank\">".LANG_LINKCAPTION_PLAYBACK."</a> <a href=\"#\" onclick=\"LoadInputControl(".$idEntry.");\">".LANG_LINKCAPTION_EDIT."</a> <a href=\"#\" onclick=\"ToggleRevisions();\">".LANG_LINKCAPTION_REVISIONS."</a>\n";
            }

            echo "        <div id=\"revisions\" style=\"display: none;\">\n".
                 "          <table border=\"1\">\n".
                 "            <thead>\n".
                 "              <tr>\n".
                 "                <th>".LANG_TABLEHEADERCAPTION_TIMESTAMPUTC."</th>\n".
                 "                <th>".LANG_TABLEHEADERCAPTION_AUTHOR."</th>\n".
                 "                <th>".LANG_TABLEHEADERCAPTION_VERSION."</th>\n".
                 "              </tr>\n".
                 "            </thead>\n".
                 "            <tbody>\n";
        }

        if ($entry['entry_revisions_revision_datetime'] != null)
        {
            echo "              <tr>\n".
                "                <td>".$entry['entry_revisions_revision_datetime']."Z</td>\n";

            if (((int)$entry['entry_revisions_id_users']) === ((int)$_SESSION['user_id']))
            {
                echo "                <td>you</td>\n";
            }
            else
            {
                echo "                <td></td>\n";
            }

            echo "                <td>".htmlspecialchars($entry['entry_revisions_text'], ENT_XHTML, "UTF-8")."</td>\n".
                "              </tr>\n";
        }
    }

    echo "            </tbody>\n".
         "          </table>\n".
         "        </div>\n";
}

echo "        <ul>\n";

if (count($entries) > 0)
{
    $lastEntryId = -1;

    foreach ($entries as $entry)
    {
        /*
        if ((int)$entry['entry_revisions_id_users'] !== (int)$_SESSION['user_id'] &&
            (int)$_SESSION['user_role'] !== USER_ROLE_ADMIN)
        {
            continue;
        }
        */

        if ($lastEntryId != (int)$entry['entries_id'])
        {
            $lastEntryId = (int)$entry['entries_id'];

            if ($idEntry !== null &&
                $lastEntryId == $idEntry)
            {
                continue;
            }

            echo "          <li>\n";

            if (strlen($entry['entry_revisions_text']) > 0)
            {
                echo "            <span>".htmlspecialchars($entry['entry_revisions_text'], ENT_XHTML, "UTF-8")."</span> <a href=\"playback.php?id=".$lastEntryId."\" target=\"_blank\">".LANG_LINKCAPTION_SUBENTRYPLAYBACK."</a> <a href=\"entry.php?id=".$lastEntryId."\">".LANG_LINKCAPTION_OPENSUBENTRY."</a>\n";
            }
            else
            {
                echo "            <span>".LANG_ENTRYDEFAULTCAPTION."</span> <a href=\"playback.php?id=".$lastEntryId."\" target=\"_blank\">".LANG_LINKCAPTION_SUBENTRYPLAYBACK."</a> <a href=\"entry.php?id=".$lastEntryId."\">".LANG_LINKCAPTION_OPENSUBENTRY."</a>\n";
            }

            echo "          </li>\n";
        }
    }
}

$queryString = "";

if ($idEntry !== null)
{
    $queryString = "?parent-id=".$idEntry;
}

echo "          <li>\n";

if ($idEntry !== null)
{
    echo "            <div>\n".
         "              <a href=\"client.php".$queryString."\">".LANG_LINKCAPTION_NEWENTRYREPLY."</a>\n".
         "            </div>\n";
}
else
{
    echo "            <div>\n".
         "              <a href=\"client.php\">".LANG_LINKCAPTION_NEWENTRYADD."</a>\n".
         "            </div>\n";
}

echo "          </li>\n".
     "        </ul>\n".
     "      </div>\n".
     "    </div>\n".
     "    <div class=\"footerbox\">\n".
     "      <a href=\"license.php\" class=\"footerbox_link\">".LANG_LICENSE."</a>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>
