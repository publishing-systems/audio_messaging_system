<?php
/* Copyright (C) 2020-2022 Stephan Kreutzer
 *
 * This file is part of audio_messaging_system.
 *
 * audio_messaging_system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * audio_messaging_system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with audio_messaging_system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/de/entry.lang.php
 * @author Stephan Kreutzer
 * @since 2020-03-26
 */



define("LANG_PAGETITLE", "Einträge");
define("LANG_HEADER", "Einträge");
define("LANG_LINKCAPTION_LEVELUP", "Hoch");
define("LANG_LINKCAPTION_MAIN", "Hauptmenü");
define("LANG_LINKCAPTION_EDIT", "Bearbeiten");
define("LANG_LINKCAPTION_REVISIONS", "Versionshistorie");
define("LANG_LINKCAPTION_DELETE", "Löschen");
define("LANG_ENTRYDEFAULTCAPTION", "Eintrag");
define("LANG_TABLEHEADERCAPTION_TIMESTAMPUTC", "Zeitstempel (UTC)");
define("LANG_TABLEHEADERCAPTION_AUTHOR", "Autor");
define("LANG_TABLEHEADERCAPTION_VERSION", "Version");
define("LANG_LINKCAPTION_PLAYBACK", "Abspielen");
define("LANG_LINKCAPTION_SAVEAS", "Speichern");
define("LANG_LINKCAPTION_SUBENTRYPLAYBACK", "Abspielen");
define("LANG_LINKCAPTION_SUBENTRYSAVEAS", "Speichern");
define("LANG_LINKCAPTION_OPENSUBENTRY", "Öffnen");
define("LANG_LINKCAPTION_ADDRECORDING", "Aufnahme hinzufügen");
define("LANG_LINKCAPTION_ADDROOM", "Raum hinzufügen");
define("LANG_LICENSE", "Lizenzierung");



?>
