<?php
/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of audio_messaging_system.
 *
 * audio_messaging_system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * audio_messaging_system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with audio_messaging_system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/recording_delete.lang.php
 * @author Stephan Kreutzer
 * @since 2022-09-04
 */



define("LANG_PAGETITLE", "Delete Recording");
define("LANG_HEADER", "Delete Recording");
define("LANG_DELETEQUESTION", "Do you really want to completely and irrecoverably delete this recording?");
define("LANG_CHECKBOXLABEL_CONFIRMYES", "yes");
define("LANG_BUTTONCAPTION_SUBMITBUTTON", "confirm");
define("LANG_DELETESUCCESS", "Successfully deleted.");
define("LANG_DELETEERROR", "Failed!");
define("LANG_LINKCAPTION_BACKTOENTRY", "back");
define("LANG_LICENSE", "Licensing");



?>
