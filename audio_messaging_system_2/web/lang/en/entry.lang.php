<?php
/* Copyright (C) 2020-2022 Stephan Kreutzer
 *
 * This file is part of audio_messaging_system.
 *
 * audio_messaging_system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * audio_messaging_system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with audio_messaging_system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/entry.lang.php
 * @author Stephan Kreutzer
 * @since 2020-03-26
 */



define("LANG_PAGETITLE", "Entries");
define("LANG_HEADER", "Entries");
define("LANG_LINKCAPTION_LEVELUP", "Up");
define("LANG_LINKCAPTION_MAIN", "Main");
define("LANG_LINKCAPTION_EDIT", "Edit");
define("LANG_LINKCAPTION_REVISIONS", "Revisions");
define("LANG_LINKCAPTION_DELETE", "Delete");
define("LANG_ENTRYDEFAULTCAPTION", "Entry");
define("LANG_TABLEHEADERCAPTION_TIMESTAMPUTC", "Timestamp (UTC)");
define("LANG_TABLEHEADERCAPTION_AUTHOR", "Author");
define("LANG_TABLEHEADERCAPTION_VERSION", "Version");
define("LANG_LINKCAPTION_PLAYBACK", "Play");
define("LANG_LINKCAPTION_SAVEAS", "Save");
define("LANG_LINKCAPTION_SUBENTRYPLAYBACK", "Play");
define("LANG_LINKCAPTION_SUBENTRYSAVEAS", "Save");
define("LANG_LINKCAPTION_OPENSUBENTRY", "Open");
define("LANG_LINKCAPTION_ADDRECORDING", "Add Recording");
define("LANG_LINKCAPTION_ADDROOM", "Add Room");
define("LANG_LICENSE", "Licensing");



?>
