<?php
/* Copyright (C) 2020-2022 Stephan Kreutzer
 *
 * This file is part of audio_messaging_system.
 *
 * audio_messaging_system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * audio_messaging_system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with audio_messaging_system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/client.lang.php
 * @author Stephan Kreutzer
 * @since 2020-03-26
 */



define("LANG_PAGETITLE", "Recording");
define("LANG_HEADER", "Recording");
define("LANG_LINKCAPTION_INITIALIZERECORDER", "Initialize recorder");
define("LANG_BUTTONCAPTION_RECORDSTART", "record");
define("LANG_BUTTONCAPTION_RECORDSTOP", "stop");
define("LANG_BUTTONCAPTION_SUBMITSAVE", "complete");
define("LANG_LINKCAPTION_BACKTOENTRY", "Back");
define("LANG_LINKCAPTION_BACKTOENTRIES", "Back");
define("LANG_LICENSE", "Licensing");



?>
