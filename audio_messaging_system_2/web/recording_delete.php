<?php
/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of audio_messaging_system.
 *
 * audio_messaging_system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * audio_messaging_system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with audio_messaging_system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/recording_delete.php
 * @author Stephan Kreutzer
 * @since 2022-09-04
 */



require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");
require_once("./libraries/user_defines.inc.php");



$entryId = null;

if (isset($_GET['id']) !== true)
{
    http_response_code(400);
    exit(0);
}

$entryId = (int)$_GET['id'];

require_once("./libraries/recording_defines.inc.php");
require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    exit(-1);
}

$entry = Database::Get()->Query("SELECT `internal_file_name`,\n".
                                "    `status`,\n".
                                "    `id_users`,\n".
                                "    `id_entries`\n".
                                "FROM `".Database::Get()->GetPrefix()."entries`\n".
                                "WHERE `id`=?\n",
                                array($entryId),
                                array(Database::TYPE_INT));

if (is_array($entry) !== true)
{
    http_response_code(500);
    exit(-1);
}

if (count($entry) <= 0)
{
    http_response_code(404);
    exit(0);
}

$entry = $entry[0];

if ((int)($entry['id_users']) != $_SESSION['user_id'] &&
    (int)($_SESSION['user_role']) !== USER_ROLE_ADMIN)
{
    http_response_code(403);
    exit(0);
}

if ((int)($entry['id_entries']) == null)
{
    http_response_code(400);
    exit(0);
}

if ((int)($entry['status']) != RECORDING_STATUS_RECORDINGCOMPLETE)
{
    http_response_code(400);
    exit(0);
}

$deleted = null;

if (isset($_POST['confirmed']) === true)
{
    if (Database::Get()->BeginTransaction() !== true)
    {
        http_response_code(500);
        exit(-1);
    }

    $deleted = Database::Get()->Execute("UPDATE `".Database::Get()->GetPrefix()."entries`\n".
                                        "SET `status`=?\n".
                                        "WHERE `id`=?\n",
                                        array(RECORDING_STATUS_DELETED, $entryId),
                                        array(Database::TYPE_INT, Database::TYPE_INT));

    if ($deleted !== true)
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        exit(-1);
    }

    if (@unlink(dirname(__FILE__)."/recordings/".$entry['internal_file_name'].".rec") !== true)
    {
        $deleted = false;
    }

    if ($deleted === true)
    {
        if (Database::Get()->CommitTransaction() !== true)
        {
            Database::Get()->RollbackTransaction();
            http_response_code(500);
            exit(-1);
        }
    }
    else
    {
        Database::Get()->RollbackTransaction();
    }
}



require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("recording_delete"));

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n";

if ($deleted === null)
{
    echo "        <p>\n".
         "          ".LANG_DELETEQUESTION."\n".
         "        </p>\n".
         "        <form action=\"recording_delete.php?id=".$entryId."\" method=\"post\">\n".
         "          <fieldset>\n".
         "            <input type=\"checkbox\" id=\"confirmation_checkbox\" name=\"confirmed\"/>\n".
         "            <label for=\"confirmation_checkbox\">".LANG_CHECKBOXLABEL_CONFIRMYES."</label><br/>\n".
         "            <input type=\"submit\" value=\"".LANG_BUTTONCAPTION_SUBMITBUTTON."\"/><br/>\n".
         "          </fieldset>\n".
         "        </form>\n".
         "        <div>\n".
         "          <a href=\"entry.php?id=".$entryId."\">".LANG_LINKCAPTION_BACKTOENTRY."</a>\n".
         "        </div>\n";
}
else if ($deleted === true)
{
    echo "        <p class=\"success\">\n".
         "          ".LANG_DELETESUCCESS."\n".
         "        </p>\n".
         "        <div>\n".
         "          <a href=\"entry.php?id=".(int)($entry['id_entries'])."\">".LANG_LINKCAPTION_BACKTOENTRY."</a>\n".
         "        </div>\n";
}
else if ($deleted === false)
{
    echo "        <p class=\"error\">\n".
         "          ".LANG_DELETEERROR."\n".
         "        </p>\n".
         "        <div>\n".
         "          <a href=\"entry.php?id=".$entryId."\">".LANG_LINKCAPTION_BACKTOENTRY."</a>\n".
         "        </div>\n";
}

echo "      </div>\n".
     "    </div>\n".
     "    <div class=\"footerbox\">\n".
     "      <a href=\"license.php\" class=\"footerbox_link\">".LANG_LICENSE."</a>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n";

?>
