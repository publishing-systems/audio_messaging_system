<?php
/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of audio_messaging_system.
 *
 * audio_messaging_system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * audio_messaging_system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with audio_messaging_system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/libraries/recording_lib.inc.php
 * @author Stephan Kreutzer
 * @since 2022-09-07
 */

function resolveMimetypeToFilenameExtension($mimetype)
{
    $filenameExtension = null;

    if (strpos($mimetype, "audio/ogg") === 0)
    {
        $filenameExtension = ".oga";
    }
    else if (strpos($mimetype, "audio/webm") === 0)
    {
        $filenameExtension = ".webm";
    }
    else if (strpos($mimetype, "audio/mp4") === 0)
    {
        $filenameExtension = ".mp4";
    }
    else
    {
        $filenameExtension = null;
    }

    return $filenameExtension;
}

?>
