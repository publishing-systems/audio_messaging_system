<?php
/* Copyright (C) 2020-2022 Stephan Kreutzer
 *
 * This file is part of audio_messaging_system.
 *
 * audio_messaging_system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * audio_messaging_system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with audio_messaging_system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/recorder.php
 * @author Stephan Kreutzer
 * @since 2020-03-18
 */



require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");

$parentEntryId = null;
$idEntryNew = null;

if (isset($_GET['parent-id']) === true)
{
    $parentEntryId = (int)$_GET['parent-id'];
}

if ($parentEntryId == null)
{
    header("HTTP/1.1 400 Bad Request");
    exit(-1);
}


if (isset($_SERVER["CONTENT_TYPE"]) !== true)
{
    http_response_code(403);
    exit(0);
}

/** @todo Filter mimetypes. */

$chunkFirst = false;
$chunkLast = false;

if (isset($_GET['start']) === true ||
    isset($_GET['end']) === true)
{
    if (isset($_GET['start']) === true)
    {
        // Reset always.
        $_SESSION['recording_id'] = md5(uniqid(rand(), true));
        $_SESSION['recording_mimetype'] = $_SERVER["CONTENT_TYPE"];

        $chunkFirst = true;
    }

    if (isset($_GET['end']) === true)
    {
        $chunkLast = true;

        if (isset($_SESSION['recording_id']) !== true)
        {
            // Why? How?

            $_SESSION['recording_id'] = md5(uniqid(rand(), true));
            $_SESSION['recording_mimetype'] = $_SERVER["CONTENT_TYPE"];
            $chunkFirst = true;
        }
    }
}
else
{
    if (isset($_SESSION['recording_id']) !== true)
    {
        $_SESSION['recording_id'] = md5(uniqid(rand(), true));
        $_SESSION['recording_mimetype'] = $_SERVER["CONTENT_TYPE"];
        $chunkFirst = true;
    }
    else
    {
        if ($_SESSION['recording_mimetype'] != $_SERVER["CONTENT_TYPE"])
        {
            /** @todo How to react to mimetype mismatch during a recording? */
        }
    }
}

$source = @fopen("php://input", "r");
$chunkWritten = false;

if ($source != false)
{
    $destination = @fopen("./recordings/".$_SESSION['recording_id'].".rec", "a");

    if ($destination != false)
    {
        while (true)
        {
            $chunk = @fread($source, 1024);

            if ($chunk == false)
            {
                break;
            }

            @fwrite($destination, $chunk);
        }

        $chunkWritten = true;

        @fclose($destination);
    }
    else
    {
        http_response_code(500);
    }

    @fclose($source);
}
else
{
    http_response_code(403);
}

if ($chunkWritten !== true)
{
    exit(-1);
}


if ($chunkFirst === true)
{
    require_once("./libraries/database.inc.php");

    if (Database::Get()->IsConnected() !== true)
    {
        header("HTTP/1.1 500 Internal Server Error");
        exit(-1);
    }

    $idEntryNew = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."entries` (`id`,\n".
                                          "    `internal_file_name`,\n".
                                          "    `mimetype`,\n".
                                          "    `status`,\n".
                                          "    `id_users`,\n".
                                          "    `id_entries`)\n".
                                          "VALUES (?, ?, ?, ?, ?, ?)",
                                          array(NULL, $_SESSION['recording_id'], $_SESSION['recording_mimetype'], ($chunkLast === true ? 2 : 1), $_SESSION['user_id'], $parentEntryId),
                                          array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT));

    if ($idEntryNew <= 0)
    {
        header("HTTP/1.1 500 Internal Server Error");
        exit(-1);
    }
}

if ($chunkLast === true &&
    $chunkFirst !== true)
{
    require_once("./libraries/database.inc.php");

    if (Database::Get()->IsConnected() !== true)
    {
        header("HTTP/1.1 500 Internal Server Error");
        exit(-1);
    }

    /** @todo $idEntryNew could also be always saved in the session and sent
      * back on every request. */
    if ($idEntryNew == null)
    {
        $entry = Database::Get()->Query("SELECT `id`\n".
                                        "FROM `".Database::Get()->GetPrefix()."entries`\n".
                                        "WHERE `internal_file_name` LIKE ?\n",
                                        array($_SESSION['recording_id']),
                                        array(Database::TYPE_STRING));

        if (is_array($entry) !== true)
        {
            header("HTTP/1.1 500 Internal Server Error");
            exit(-1);
        }

        if (count($entry) <= 0)
        {
            header("HTTP/1.1 500 Internal Server Error");
            exit(-1);
        }

        $idEntryNew = (int)$entry[0]['id'];

        if ($idEntryNew <= 0)
        {
            header("HTTP/1.1 500 Internal Server Error");
            exit(-1);
        }
    }

    $success = Database::Get()->Execute("UPDATE `".Database::Get()->GetPrefix()."entries`\n".
                                        "SET `status`=?\n".
                                        "WHERE `id`=?",
                                        array(2, $idEntryNew),
                                        array(Database::TYPE_INT, Database::TYPE_INT));

    if ($success !== true)
    {
        header("HTTP/1.1 500 Internal Server Error");
        exit(-1);
    }
}

if ($chunkLast == true)
{
    if ($idEntryNew != null)
    {
        echo $idEntryNew;
    }
    else
    {
        header("HTTP/1.1 500 Internal Server Error");
        exit(-1);
    }
}



?>
