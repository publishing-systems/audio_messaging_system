/* Copyright (C) 2020-2022 Stephan Kreutzer
 *
 * This file is part of audio_messaging_system.
 *
 * audio_messaging_system is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * audio_messaging_system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with audio_messaging_system. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let xmlhttp = null;

// Mozilla
if (window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
// IE
else if (window.ActiveXObject)
{
    xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
}

if (xmlhttp == null)
{
    // TODO
}

function initRecorder()
{
    let initLink = document.getElementById('initLink');

    if (initLink != null)
    {
        initLink.parentNode.removeChild(initLink);
    }

    let buttonRecorderStart = document.getElementById("buttonRecorderStart");
    let buttonRecorderStop = document.getElementById("buttonRecorderStop");

    if (buttonRecorderStart == null)
    {
        console.log("Button with ID 'buttonRecorderStart' is missing. Abort.");
        return -1;
    }

    if (buttonRecorderStop == null)
    {
        console.log("Button with ID 'buttonRecorderStop' is missing. Abort.");
        return -1;
    }

    if (mediaRecorder != null)
    {
        if (mediaRecorder.state == "running")
        {
            buttonRecorderStart.disabled = true;
            buttonRecorderStop.disabled = false;
        }
        else
        {
            buttonRecorderStop.disabled = true;
            buttonRecorderStart.disabled = false;
        }
    }
    else
    {
        buttonRecorderStop.disabled = true;
        buttonRecorderStart.disabled = false;
    }

    return 0;
}

window.onload = function () {
    initRecorder();
};



let mediaRecorder = null;
let parentId = null;
let recorderChunkFirst = false;
let recorderChunkLast = false;

async function recorderStart(parentEntryId)
{
    recorderChunkFirst = true;
    parentId = parentEntryId;

    if (mediaRecorder == null)
    {
        if (!!!navigator.mediaDevices)
        {
            errorMessage("No navigator.mediaDevices.");
            return -1;
        }

        if (!!!navigator.mediaDevices.getUserMedia)
        {
            errorMessage("No navigator.mediaDevices.getUserMedia.");
            return -1;
        }

        let stream = null;

        try
        {
            stream = await navigator.mediaDevices.getUserMedia({ audio: true });

            if (MediaRecorder.isTypeSupported("audio/ogg;codecs=opus") == true)
            {
                mediaRecorder = new MediaRecorder(stream, { "mimeType": "audio/ogg;codecs=opus" });
            }
            else
            {
                mediaRecorder = new MediaRecorder(stream);
            }

            mediaRecorder.ondataavailable = function(e) {
                request(e.data, mediaRecorder.mimeType, parentId, recorderChunkFirst, recorderChunkLast);

                if (recorderChunkFirst == true)
                {
                    recorderChunkFirst = false;
                }

                if (recorderChunkLast == true)
                {
                    // Start recording again on the same instance isn't allowed, because
                    // the user has to be aware and explicitly consent to another recording
                    // being made, either by granting permission again manually or as a
                    // general permission for the site.
                    mediaRecorder = null;

                    recorderChunkLast = false;
                }
            }

            mediaRecorder.onstop = function(e) {
                stream.getTracks().forEach(function(track) {
                    track.stop();
                });
            }
        }
        catch (ex)
        {
            errorMessage(ex.message);

            mediaRecorder = null;
            return -1;
        }
    }
    else
    {
        if (mediaRecorder.state == "running")
        {
            recorderStop();
        }
    }

    //mediaRecorder.start();
    mediaRecorder.start(10000);  // Interval for ondataavailable() event.

    {
        let buttonRecorderStart = document.getElementById("buttonRecorderStart");
        let buttonRecorderStop = document.getElementById("buttonRecorderStop");

        if (buttonRecorderStart != null)
        {
            buttonRecorderStart.disabled = true;
        }
        else
        {
            console.log("Button with ID 'buttonRecorderStart' is missing.");
        }

        if (buttonRecorderStop != null)
        {
            buttonRecorderStop.disabled = false;
        }
        else
        {
            console.log("Button with ID 'buttonRecorderStop' is missing.");
        }
    }

    return 0;
}

// TODO: recorderPause(), recorderContinue().

function recorderStop()
{
    recorderChunkLast = true;

    if (mediaRecorder != null)
    {
        mediaRecorder.stop();
    }

    {
        let buttonRecorderStart = document.getElementById("buttonRecorderStart");
        let buttonRecorderStop = document.getElementById("buttonRecorderStop");

        if (buttonRecorderStop != null)
        {
            buttonRecorderStop.disabled = true;
        }
        else
        {
            console.log("Button with ID 'buttonRecorderStop' is missing.");
        }

        if (buttonRecorderStart != null)
        {
            // buttonRecorderStart.disabled = false;
        }
        else
        {
            console.log("Button with ID 'buttonRecorderStart' is missing.");
        }
    }
}

function errorMessage(message)
{
    {
        let buttonRecorderStart = document.getElementById("buttonRecorderStart");
        let buttonRecorderStop = document.getElementById("buttonRecorderStop");

        if (buttonRecorderStart != null)
        {
            buttonRecorderStart.disabled = true;
        }
        else
        {
            console.log("Button with ID 'buttonRecorderStart' is missing.");
        }

        if (buttonRecorderStop != null)
        {
            buttonRecorderStop.disabled = true;
        }
        else
        {
            console.log("Button with ID 'buttonRecorderStop' is missing.");
        }
    }

    let container = document.getElementById("container");

    if (container != null)
    {
        let p = document.createElement("p");
        p.setAttribute("class", "error");

        let textNode = document.createTextNode(message);
        p.appendChild(textNode);

        container.appendChild(p);
    }
    else
    {
        console.log("Element with ID 'container' is missing.");
    }
}

function request(data, mimetype, parentId, isFirstChunk, isLastChunk)
{
    if (xmlhttp != null &&
        data != null)
    {
        let requestPath = "";

        {
            let scripts = document.getElementsByTagName("script");
            // 'scripts' contains always the last script tag that was loaded.
            let myPath = scripts[scripts.length-1].src;

            requestPath = myPath.substring(0, myPath.lastIndexOf('/'));
            requestPath += "/recorder.php";
        }

        let parameters = "";

        if (parentId != null)
        {
            if (parameters.length > 0)
            {
              parameters += "&";
            }

            parameters += "parent-id=";
            parameters += parseInt(parentId);
        }

        if (isFirstChunk == true)
        {
            if (parameters.length > 0)
            {
              parameters += "&";
            }

            parameters += "start=true";
        }

        if (isLastChunk == true)
        {
            if (parameters.length > 0)
            {
              parameters += "&";
            }

            parameters += "end=true";
        }

        if (parameters.length > 0)
        {
            requestPath += "?";
            requestPath += parameters;
        }

        // 'file://' is bad.
        if (requestPath.substring(0, 7) == "file://")
        {
            requestPath = requestPath.substr(8);
            requestPath = "https://" + requestPath;
        }

        xmlhttp.open('PUT', requestPath, true);
        // Cookie is submitted automatically.

        if (mimetype != null)
        {
            xmlhttp.setRequestHeader('Content-Type',
                                     mimetype);
        }

        xmlhttp.onreadystatechange = result;
        xmlhttp.send(data);
    }
    else
    {
        // TODO
    }
}

function result()
{
    if (xmlhttp.readyState != 4)
    {
        // Waiting...
    }

    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
        if (xmlhttp.responseText == '')
        {
            return;
        }

        let newEntryId = parseInt(xmlhttp.responseText);
        let formRecorder = document.getElementById("formRecorder");

        if (formRecorder != null)
        {
            formRecorder.setAttribute("action", "entry.php?id=" + newEntryId);

            let buttonComplete = document.getElementById("buttonComplete");

            if (buttonComplete != null)
            {
                buttonComplete.disabled = false;
            }
            else
            {
                console.log("Element with ID 'buttonComplete' is missing.");
            }
        }
        else
        {
            console.log("Element with ID 'formRecorder' is missing.");
        }
    }
    else if (xmlhttp.readyState == 4 && xmlhttp.status == 0)
    {
        alert("Offline...");
    }
}
